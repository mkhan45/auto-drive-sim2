using UnityEngine;
using System.Collections;


namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof(CarController))]
    public class AI_CarControl : MonoBehaviour
    {   

        public bool isPlayer;
        
        public bool randomDriving;

        public int randomDrivingLoopsBeforeLaneChange = 1000;

        private CarController m_Car;
        private Steering s;

        [Header("Sensors")]
        public float sensorLength = 30f;
        public Vector3 frontSensorPosition = new Vector3(0f, 0.2f, 0.5f);

        public Vector3 sideSensorPosition = new Vector3(0f,0f,0f);
        public float frontSideSensorPosition = 0.2f;
        public float frontSensorAngle = 30f;

        public float sidePersonalSpaceMargin = 10f;

        public float carWidth = 20;

        private float maxSteerAngle;

        public float minSteerAngle = 0f;

        public float maxPidTargetChangeInOneFrame = 0.2f;

        public float pathAttenuation = 1.01f;

        private float targetSteerAngle = 0;

        private float prevError = 1, PIDOutput = 0, pidTarget = 0f, prevCenterDistance = -1, prevTargPos = 0;

        private float leftDist = 30, frontLeftAngleDist = 30, centerDist = 30, frontRightAngleDist = 30, rightDist = 30; //the readings of all the sensors

        private float throttle = 1, brake = 0;

        private bool objectInfront = false, passing = false, attenuatePIDTarget = true;

        private long loops = 0;
        private float swerveDirection = 1.0f;
        private float trackWidth = 0;



        private float randPidTargetSet = 0f;


        private void Awake()
        {

            m_Car = GetComponent<CarController>();
            
            //Custom init
            maxSteerAngle = m_Car.m_MaximumSteerAngle;
          

            s = new Steering();
            s.Start();

     
            

        }

        private void FixedUpdate()
        {
            s.UpdateValues();

            if (isPlayer) {
                SensorsV4();
            }else{
                SensorsV2();
            }

            m_Car.Move(targetSteerAngle, throttle, brake, brake);
            //m_Car.Move(s.H, s.V, s.V, 0f);

        }

        private void SensorsV4() {
            /*
            Specs
            */

        //initialize
            //set up variables
            RaycastHit hit;
            Vector3 sensorStartPos = transform.position;
            sensorStartPos += transform.forward * frontSensorPosition.z;
            sensorStartPos += transform.up * frontSensorPosition.y;

            Vector3 sideSensorPos = transform.position;
            sideSensorPos += transform.forward * sideSensorPosition.z;
            sideSensorPos += transform.up * sideSensorPosition.y;
            
            float position = 0, pError = 0;


            centerDist = 100000000000000;
            
            //increase loops for functions that change over time
            loops++;


        
    // find the position of the car

        //get ray distance data

            //front right angle sensor
            if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    frontRightAngleDist = hit.distance;
                }
            }

            

            //front left angle sensor
            if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(-frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    frontLeftAngleDist = hit.distance;
                }
            }

            
            //leftSensor
            if (Physics.Raycast(sideSensorPos, Quaternion.AngleAxis(-90, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    leftDist = hit.distance;
                }
            }

            //right sensor
            if (Physics.Raycast(sideSensorPos, Quaternion.AngleAxis(90, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    rightDist = hit.distance;
                }
            }



            float frontFacingSensorLength = 2*sensorLength; //the front-facing sensors need to see further

            //front center sensor
            if (Physics.Raycast(sensorStartPos, transform.forward, out hit, frontFacingSensorLength)) {  
                if (hit.collider.CompareTag("CPU")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    if (hit.distance < centerDist) {
                        centerDist = hit.distance;
                    }
                }
            }

             //forward-facing right sensor
            sensorStartPos += transform.right * frontSideSensorPosition;
                //these sensors are angled in slightly so that they dont detect the walls while we are turning
            if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(-2, transform.up) * transform.forward, out hit, frontFacingSensorLength)) {   
                if (hit.collider.CompareTag("CPU")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    if (hit.distance < centerDist) {
                        centerDist = hit.distance;
                    }
                }
            }


            //forward-facing left sensor
                //these sensors are angled in slightly so that they dont detect the walls while we are turning
            sensorStartPos -= transform.right * frontSideSensorPosition * 2;
            if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(2, transform.up) * transform.forward, out hit, frontFacingSensorLength)) {
                if (hit.collider.CompareTag("CPU")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    if (hit.distance < centerDist) {
                        centerDist = hit.distance;
                    }
                }
            }

    

    //figure out what the pid target should be

            //figure out whether there is an object infront of the car
            if ((centerDist < frontLeftAngleDist*1.5) && (centerDist < frontRightAngleDist*1.5)) {
                objectInfront = true;
            }else{
                objectInfront = false;
            }
            
            
            //Calculate width of track (average over the first 100 loops)
            if (loops < 100) {
                trackWidth *= loops-1;
                trackWidth += (leftDist+rightDist+carWidth);
                trackWidth /= loops;
            }



            //check if there is an object in front of us, and if so - swerve!
            if (objectInfront)  {
                pidTarget -= swerveDirection*maxPidTargetChangeInOneFrame;// (swerveDirection*1-pidTarget)/2;
                passing = true;
                attenuatePIDTarget = false;

                // slow down to try and match the car infront of us's speed
                if (prevCenterDistance != -1f) {
                    if (centerDist-prevCenterDistance < 5) {
                        throttle += (1f-throttle/2f);
                        brake /= 2f;
                    }else{ 
                        brake += (1f-brake)/2f;
                        throttle /= 2f;
                    }
                }else{
                    throttle = 0;
                    brake = 1;
                }
                prevCenterDistance = centerDist;                
            }else{
                throttle = 1;
                brake = 0;
                prevCenterDistance = -1;
            }
            

            // check if there is something too close beside us
            /*if (! objectInfront) {
                if (leftDist < sidePersonalSpaceMargin) {
                    pidTarget += 1f * (sidePersonalSpaceMargin-leftDist)/(trackWidth/2-carWidth);
                }
                if (rightDist < sidePersonalSpaceMargin) {
                    pidTarget -= 1f * (sidePersonalSpaceMargin-rightDist)/(trackWidth/2-carWidth);
                }
            }*/

            position = frontRightAngleDist+rightDist-leftDist-frontLeftAngleDist;            


            // make sure we dont change the pid target too much in one frame (which will cause the car to lose control)
            if (Mathf.Abs(prevTargPos-pidTarget) > maxPidTargetChangeInOneFrame) {
                Debug.Log("capped pid target change");
                pidTarget = prevTargPos + maxPidTargetChangeInOneFrame * Mathf.Abs(prevTargPos-pidTarget)/(pidTarget-prevTargPos);
            }
            prevTargPos = pidTarget;


            // if we are trying to swerve and we hit the edge of the track, try swerving the other way
            if ((objectInfront) && (Mathf.Abs(pidTarget) >= 0.9)) {
                swerveDirection *= -1f;
                pidTarget = 0.8999f * Mathf.Abs(pidTarget)/pidTarget;
            }


            //determine whether we should be driving randomly, falling back to the center, or neither
            if ( (! passing) && randomDriving) { //if we want to drive randomly
                if (loops % randomDrivingLoopsBeforeLaneChange == 0) {
                    randPidTargetSet = Mathf.Sqrt(Random.Range(0.0f,1f));
                    randPidTargetSet *= Random.Range(0f,1f) < 0.5 ? -1f: 1f;
                }
                pidTarget = randPidTargetSet;
            }else if (attenuatePIDTarget) {
                pidTarget /= pathAttenuation; // fall back towards the center line
            }else if ( Mathf.Abs((leftDist + rightDist) - trackWidth) < carWidth ) { //we are beside something
                Debug.Log("not passing");
                passing = false;
            }else if (! passing) { // we have passed the thing we were passing
                attenuatePIDTarget = true;
            }

            if (loops > 1000000000000000L) loops = 100000L; //make sure we don't exceed max range of long


            
    // PID controller - determine the steering angle

            float pidTargPos = pidTarget * (trackWidth-carWidth/2); // convert the -1 to 1 float of pidTarget to an actual location on the track

            float error = position-pidTargPos; //the distance we are away from where we want to be 


            // if our error is too big the pid will try and turn the car so rapidly we lose control. Cap the error so that doesn't happen
            if (Mathf.Abs(error) > maxPidTargetChangeInOneFrame) {
                pError = maxPidTargetChangeInOneFrame * error/Mathf.Abs(error);
            }

            //pid controller
            PIDOutput = 0.1f*pError + 0.3f * (error-prevError);
            
            //set the output of the pid controller to 
            if ( Mathf.Abs(maxSteerAngle * PIDOutput) < minSteerAngle) { 
                //if we are turning less than the min steering angle, don't turn at all
                targetSteerAngle = 0;
            }else{
                //otherwise output normally
                targetSteerAngle = maxSteerAngle * PIDOutput;
            }

            prevError = error; // set the previous error to this loop's error   --   for derivative terms and for capping the max error
        }

        private void SensorsV3() { //also old
    
            /*
            Specs:
            cruising speed: 100mph
            max speed: 150+ mph
            Avoidance: Straights and turns with speed 15 < dif < 35 mph
            */

        //initialize
            //set up variables
            RaycastHit hit;
            Vector3 sensorStartPos = transform.position;
            sensorStartPos += transform.forward * frontSensorPosition.z;
            sensorStartPos += transform.up * frontSensorPosition.y;
            
            float position = 0, pError = 0;
            
            //increase loops for functions that change over time
            loops++;


        //get ray distance data
            //front right sensor
            sensorStartPos += transform.right * frontSideSensorPosition;
            if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    rightDist = hit.distance;
                }
            }

            //front right angle sensor
            else if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    frontRightAngleDist = hit.distance;
                }
            }

            //front left sensor
            sensorStartPos -= transform.right * frontSideSensorPosition * 2;
            if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    leftDist = hit.distance;
                }
            }

            //front left angle sensor
            else if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(-frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    frontLeftAngleDist = hit.distance;
                }
            }

            //front center sensor
            float centerSensorLength = 2*sensorLength;// sensorLength*Mathf.Sin(frontSensorAngle); //make sure that angled sensors detect curves before we do

            if (Physics.Raycast(sensorStartPos, transform.forward, out hit, centerSensorLength)) { 
                    
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                        
                    centerDist = hit.distance;   

                    if ((centerDist < leftDist) && (centerDist < rightDist)) {
                        objectInfront = true;
                    }else{
                        objectInfront = false;
                    }
                }else{
                    objectInfront = false;
                    centerDist = -1f;
                    prevCenterDistance = -1f;
                }
            }else{ //there is no object within our range
                objectInfront = false;
                centerDist = -1f; 
                prevCenterDistance = -1f;
            }

            //Calculate width of track
            if (loops < 50) {
                trackWidth *= loops-1;
                trackWidth += (leftDist+rightDist);
                trackWidth /= loops;
            }



            // check if there is a car infront of us
            if (objectInfront)  {
                pidTarget -= (swerveDirection*1-pidTarget)/2; //swerveDirection*maxPidTargetChangeInOneFrame;
                passing = true;
                attenuatePIDTarget = false;
                if (prevCenterDistance != -1f) {
                    if (centerDist-prevCenterDistance < 0) {
                        throttle += (1f-throttle/2f);
                        brake /= 2f;
                    }else{ 
                        brake += (1f-brake)/2f;
                        throttle /= 2f;
                    }
                }else{
                    throttle = 0;
                    brake = 1;
                }
                prevCenterDistance = centerDist;

                
            }else{
                throttle = 1;
                brake = 0;
                prevCenterDistance = -1;
            }
            

            // check if there is something too close beside us
            if (leftDist < sidePersonalSpaceMargin) {
                pidTarget -= 1f * Mathf.Sqrt(sidePersonalSpaceMargin-leftDist)/(trackWidth/2-carWidth);
            }
            if (rightDist < sidePersonalSpaceMargin) {
                pidTarget += 1f * Mathf.Sqrt(sidePersonalSpaceMargin-rightDist)/(trackWidth/2-carWidth);
            }

            position = frontRightAngleDist+rightDist-leftDist-frontLeftAngleDist;            

    
            if (Mathf.Abs(prevTargPos-pidTarget) > maxPidTargetChangeInOneFrame) {
                Debug.Log("capped pid target change");
                pidTarget = prevTargPos + maxPidTargetChangeInOneFrame * Mathf.Abs(prevTargPos-pidTarget)/(pidTarget-prevTargPos);
            }

            if ((objectInfront) && (Mathf.Abs(pidTarget) >= 0.9)) {
                swerveDirection *= -1f;
                pidTarget = 0.8999f * Mathf.Abs(pidTarget)/pidTarget;
            }


            if ((! passing) && randomDriving) { //if we want to drive randomly
                if (loops % 1000 == 0) {
                    randPidTargetSet = Mathf.Sqrt(Random.Range(0.0f,1f));
                    randPidTargetSet *= Random.Range(0f,1f) < 0.5 ? -1: 1;
                }
                pidTarget = randPidTargetSet;
            }else if (attenuatePIDTarget) {
                pidTarget /= 1.05f; //fall back to the center
            }else if ( Mathf.Abs((leftDist + rightDist) - trackWidth) < carWidth ) { //we are beside something
                passing = false;
            }else if (! passing) { // we have passed the thing we were passing
                attenuatePIDTarget = true;
            }

            if (loops > 1000000000000000L) loops = 100000L; //make sure we don't exceed max range of long

            prevTargPos = pidTarget;

            float pidTargPos = pidTarget * (trackWidth/2-carWidth);
    
            
            float error = position-pidTargPos;

            if (Mathf.Abs(error) > maxPidTargetChangeInOneFrame) {
                pError = maxPidTargetChangeInOneFrame * error/Mathf.Abs(error);
            }

            //pid controller
            PIDOutput = 0.1f*pError + 0.3f * (error-prevError);
            
            if ( Mathf.Abs(maxSteerAngle * PIDOutput) < minSteerAngle) {
                targetSteerAngle = 0;
            }else{
                targetSteerAngle = maxSteerAngle * PIDOutput;
            }
            
            prevError = error;

        }

        
        private void SensorsV2()  {//old verson of auton
            
            /* SPECS:
                Cruising speed: 100mph
                Max speed: 120 mph
            */

            RaycastHit hit;
            Vector3 sensorStartPos = transform.position;
            sensorStartPos += transform.forward * frontSensorPosition.z;
            sensorStartPos += transform.up * frontSensorPosition.y;
            float avoidMultiplier = 0;

            //front right sensor
            sensorStartPos += transform.right * frontSideSensorPosition;
            if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    avoidMultiplier -= 10f/hit.distance;
                }
            }

            //front right angle sensor
            else if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    avoidMultiplier -= 0.5f/hit.distance;
                }
            }

            //front left sensor
            sensorStartPos -= transform.right * frontSideSensorPosition * 2;
            if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    avoidMultiplier += 10f/hit.distance;
                }
            }

            //front left angle sensor
            else if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(-frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    avoidMultiplier += 0.5f/hit.distance;
                }
            }

            //front center sensor
            if (avoidMultiplier == 0) {
                if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength)) {
                    if (!hit.collider.CompareTag("Terrain")) {
                        Debug.DrawLine(sensorStartPos, hit.point);
                        if (hit.normal.x < 0) {
                            avoidMultiplier = -0.001f;
                        } else {
                            avoidMultiplier = 0.001f;
                        }
                    }
                }
            }


            PIDOutput = 5f*(avoidMultiplier) + 10f * (avoidMultiplier-prevError);
            
            targetSteerAngle = maxSteerAngle * PIDOutput;

            prevError = avoidMultiplier;

        }


    }
}
